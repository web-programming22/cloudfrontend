import { Food } from 'src/foods/entities/food.entity';
export declare class Category {
    id: number;
    name: string;
    foods: Food[];
}
