"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const food_entity_1 = require("./foods/entities/food.entity");
const foods_module_1 = require("./foods/foods.module");
const user_entity_1 = require("./users/entities/user.entity");
const users_module_1 = require("./users/users.module");
const tables_module_1 = require("./tables/tables.module");
const table_entity_1 = require("./tables/entities/table.entity");
const material_entity_1 = require("./material/entities/material.entity");
const material_module_1 = require("./material/material.module");
const stock_entity_1 = require("./stocks/entities/stock.entity");
const stocks_module_1 = require("./stocks/stocks.module");
const stock_items_1 = require("./stocks/entities/stock-items");
const receipts_module_1 = require("./receipts/receipts.module");
const receipt_entity_1 = require("./receipts/entities/receipt.entity");
const receipt_items_1 = require("./receipts/entities/receipt-items");
const orders_module_1 = require("./orders/orders.module");
const customer_entity_1 = require("./customers/entities/customer.entity");
const order_entity_1 = require("./orders/entities/order.entity");
const order_item_1 = require("./orders/entities/order-item");
const customers_module_1 = require("./customers/customers.module");
const quesfoods_module_1 = require("./quesfoods/quesfoods.module");
const quesfood_entity_1 = require("./quesfoods/entities/quesfood.entity");
const emp_module_1 = require("./emp/emp.module");
const emp_entity_1 = require("./emp/entities/emp.entity");
const categories_module_1 = require("./categories/categories.module");
const category_entity_1 = require("./categories/entities/category.entity");
const auth_module_1 = require("./auth/auth.module");
const salary_module_1 = require("./salary/salary.module");
const salary_entity_1 = require("./salary/entities/salary.entity");
const stocksItems_module_1 = require("./stocks/stocksItems.module");
const receiptsItem_module_1 = require("./receipts/receiptsItem.module");
let AppModule = class AppModule {
    constructor(dataSource) {
        this.dataSource = dataSource;
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'db-web',
                password: 'pass1234',
                database: 'db-web',
                entities: [food_entity_1.Food,
                    user_entity_1.User,
                    table_entity_1.Table,
                    material_entity_1.Material,
                    stock_entity_1.Stock,
                    stock_items_1.StockItem,
                    receipt_entity_1.Receipt,
                    receipt_items_1.ReceiptItem,
                    customer_entity_1.Customer,
                    order_entity_1.Order,
                    order_item_1.OrderItem,
                    quesfood_entity_1.Quesfood,
                    emp_entity_1.Emp,
                    category_entity_1.Category,
                    salary_entity_1.Salary,],
                synchronize: true,
            }),
            tables_module_1.TablesModule,
            stocks_module_1.StocksModule,
            foods_module_1.FoodsModule,
            users_module_1.UsersModule,
            material_module_1.MaterialModule,
            receipts_module_1.ReceiptsModule,
            orders_module_1.OrdersModule,
            customers_module_1.CustomersModule,
            quesfoods_module_1.QuesfoodsModule,
            emp_module_1.EmpModule,
            categories_module_1.CategoriesModule,
            auth_module_1.AuthModule,
            salary_module_1.SalaryModule,
            stocksItems_module_1.StockItemsModule,
            receiptsItem_module_1.ReceiptsItemsModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    }),
    __metadata("design:paramtypes", [typeorm_2.DataSource])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map