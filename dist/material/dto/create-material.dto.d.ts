export declare class CreateMaterialDto {
    name: string;
    minAmount: number;
    unit: string;
    balance: number;
}
