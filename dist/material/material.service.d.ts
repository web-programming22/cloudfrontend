import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
export declare class MaterialService {
    private materialsRepository;
    constructor(materialsRepository: Repository<Material>);
    create(createMaterialDto: CreateMaterialDto): Promise<CreateMaterialDto & Material>;
    findAll(): Promise<Material[]>;
    findOne(id: number): Promise<Material>;
    update(id: number, updateMaterialDto: UpdateMaterialDto): Promise<{
        name?: string;
        minAmount?: number;
        unit?: string;
        balance?: number;
        id: number;
    } & Material>;
    remove(id: number): Promise<Material>;
}
