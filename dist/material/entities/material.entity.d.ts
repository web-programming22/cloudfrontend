import { ReceiptItem } from 'src/receipts/entities/receipt-items';
import { StockItem } from 'src/stocks/entities/stock-items';
export declare class Material {
    id: number;
    name: string;
    minAmount: number;
    unit: string;
    balance: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    stockItems: StockItem[];
    receiptItems: ReceiptItem[];
}
