import { MaterialService } from './material.service';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
export declare class MaterialController {
    private readonly materialService;
    constructor(materialService: MaterialService);
    create(createMaterialDto: CreateMaterialDto): Promise<CreateMaterialDto & import("./entities/material.entity").Material>;
    findAll(): Promise<import("./entities/material.entity").Material[]>;
    findOne(id: string): Promise<import("./entities/material.entity").Material>;
    update(id: string, updateMaterialDto: UpdateMaterialDto): Promise<{
        name?: string;
        minAmount?: number;
        unit?: string;
        balance?: number;
        id: number;
    } & import("./entities/material.entity").Material>;
    remove(id: string): Promise<import("./entities/material.entity").Material>;
}
