import { OrderItem } from 'src/orders/entities/order-item';
export declare class Quesfood {
    id: number;
    status: string;
    orderItems: OrderItem;
    orderItemId: number;
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
}
