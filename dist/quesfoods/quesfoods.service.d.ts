import { Food } from 'src/foods/entities/food.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { Repository } from 'typeorm';
import { CreateQuesfoodDto } from './dto/create-quesfood.dto';
import { UpdateQuesfoodDto } from './dto/update-quesfood.dto';
import { Quesfood } from './entities/quesfood.entity';
export declare class QuesfoodsService {
    private quesfoodRepository;
    private orderItemsRepository;
    private foodsRepository;
    constructor(quesfoodRepository: Repository<Quesfood>, orderItemsRepository: Repository<OrderItem>, foodsRepository: Repository<Food>);
    create(createQuesfoodDto: CreateQuesfoodDto): Promise<CreateQuesfoodDto & Quesfood>;
    findAll(): Promise<Quesfood[]>;
    findStatusQue(status: string): Promise<Quesfood[]>;
    findOne(id: number): Promise<Quesfood>;
    update(id: number, updateQuesfoodDto: UpdateQuesfoodDto): Promise<{
        status: string;
        orderItemId: number;
        id: number;
        orderItems: OrderItem;
        createdDate: Date;
        updatedDate: Date;
        deletedDate: Date;
    } & Quesfood>;
    remove(id: number): Promise<Quesfood>;
}
