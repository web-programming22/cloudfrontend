import { QuesfoodsService } from './quesfoods.service';
import { CreateQuesfoodDto } from './dto/create-quesfood.dto';
import { UpdateQuesfoodDto } from './dto/update-quesfood.dto';
export declare class QuesfoodsController {
    private readonly quesfoodsService;
    constructor(quesfoodsService: QuesfoodsService);
    create(createQuesfoodDto: CreateQuesfoodDto): Promise<CreateQuesfoodDto & import("./entities/quesfood.entity").Quesfood>;
    findAll(): Promise<import("./entities/quesfood.entity").Quesfood[]>;
    findOne(id: string): Promise<import("./entities/quesfood.entity").Quesfood>;
    update(id: string, updateQuesfoodDto: UpdateQuesfoodDto): Promise<{
        status: string;
        orderItemId: number;
        id: number;
        orderItems: import("../orders/entities/order-item").OrderItem;
        createdDate: Date;
        updatedDate: Date;
        deletedDate: Date;
    } & import("./entities/quesfood.entity").Quesfood>;
    remove(id: string): Promise<import("./entities/quesfood.entity").Quesfood>;
    findAllByStatus(status: string): Promise<import("./entities/quesfood.entity").Quesfood[]>;
}
