"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuesfoodsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const food_entity_1 = require("../foods/entities/food.entity");
const order_item_1 = require("../orders/entities/order-item");
const typeorm_2 = require("typeorm");
const quesfood_entity_1 = require("./entities/quesfood.entity");
let QuesfoodsService = class QuesfoodsService {
    constructor(quesfoodRepository, orderItemsRepository, foodsRepository) {
        this.quesfoodRepository = quesfoodRepository;
        this.orderItemsRepository = orderItemsRepository;
        this.foodsRepository = foodsRepository;
    }
    async create(createQuesfoodDto) {
        return this.quesfoodRepository.save(createQuesfoodDto);
    }
    findAll() {
        return this.quesfoodRepository.find();
    }
    findStatusQue(status) {
        return this.quesfoodRepository.find({
            relations: ['orderItems', 'orderItems.order', 'orderItems.order.table'],
            where: { status: status },
        });
    }
    async findOne(id) {
        const quesfood = await this.quesfoodRepository.findOne({
            where: { id: id },
        });
        if (!quesfood) {
            throw new common_1.NotFoundException();
        }
        return quesfood;
    }
    async update(id, updateQuesfoodDto) {
        const quesfood = await this.quesfoodRepository.findOneBy({ id: id });
        if (!quesfood) {
            throw new common_1.NotFoundException();
        }
        const updatedQuefood = Object.assign(Object.assign({}, quesfood), updateQuesfoodDto);
        return this.quesfoodRepository.save(updatedQuefood);
    }
    async remove(id) {
        const quesfood = await this.quesfoodRepository.findOne({
            where: { id: id },
        });
        try {
            const deletedQuefoos = await this.quesfoodRepository.softRemove(quesfood);
            return deletedQuefoos;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
};
QuesfoodsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(quesfood_entity_1.Quesfood)),
    __param(1, (0, typeorm_1.InjectRepository)(order_item_1.OrderItem)),
    __param(2, (0, typeorm_1.InjectRepository)(food_entity_1.Food)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], QuesfoodsService);
exports.QuesfoodsService = QuesfoodsService;
//# sourceMappingURL=quesfoods.service.js.map