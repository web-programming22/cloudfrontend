"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuesfoodsController = void 0;
const common_1 = require("@nestjs/common");
const quesfoods_service_1 = require("./quesfoods.service");
const create_quesfood_dto_1 = require("./dto/create-quesfood.dto");
const update_quesfood_dto_1 = require("./dto/update-quesfood.dto");
let QuesfoodsController = class QuesfoodsController {
    constructor(quesfoodsService) {
        this.quesfoodsService = quesfoodsService;
    }
    create(createQuesfoodDto) {
        return this.quesfoodsService.create(createQuesfoodDto);
    }
    findAll() {
        return this.quesfoodsService.findAll();
    }
    findOne(id) {
        return this.quesfoodsService.findOne(+id);
    }
    update(id, updateQuesfoodDto) {
        return this.quesfoodsService.update(+id, updateQuesfoodDto);
    }
    remove(id) {
        return this.quesfoodsService.remove(+id);
    }
    findAllByStatus(status) {
        return this.quesfoodsService.findStatusQue(status);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_quesfood_dto_1.CreateQuesfoodDto]),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_quesfood_dto_1.UpdateQuesfoodDto]),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "remove", null);
__decorate([
    (0, common_1.Get)('status/:text'),
    __param(0, (0, common_1.Param)('text')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], QuesfoodsController.prototype, "findAllByStatus", null);
QuesfoodsController = __decorate([
    (0, common_1.Controller)('quesfoods'),
    __metadata("design:paramtypes", [quesfoods_service_1.QuesfoodsService])
], QuesfoodsController);
exports.QuesfoodsController = QuesfoodsController;
//# sourceMappingURL=quesfoods.controller.js.map