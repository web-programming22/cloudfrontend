"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuesfoodsModule = void 0;
const common_1 = require("@nestjs/common");
const quesfoods_service_1 = require("./quesfoods.service");
const quesfoods_controller_1 = require("./quesfoods.controller");
const quesfood_entity_1 = require("./entities/quesfood.entity");
const typeorm_1 = require("@nestjs/typeorm");
const order_item_1 = require("../orders/entities/order-item");
const food_entity_1 = require("../foods/entities/food.entity");
let QuesfoodsModule = class QuesfoodsModule {
};
QuesfoodsModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([quesfood_entity_1.Quesfood, order_item_1.OrderItem, food_entity_1.Food])],
        controllers: [quesfoods_controller_1.QuesfoodsController],
        providers: [quesfoods_service_1.QuesfoodsService],
    })
], QuesfoodsModule);
exports.QuesfoodsModule = QuesfoodsModule;
//# sourceMappingURL=quesfoods.module.js.map