import { SalaryService } from './salary.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
export declare class SalaryController {
    private readonly salaryService;
    constructor(salaryService: SalaryService);
    create(createSalaryDto: CreateSalaryDto): Promise<CreateSalaryDto & import("./entities/salary.entity").Salary>;
    findAll(): Promise<import("./entities/salary.entity").Salary[]>;
    findOne(id: string): Promise<import("./entities/salary.entity").Salary>;
    update(id: string, updateSalaryDto: UpdateSalaryDto): Promise<{
        name?: string;
        position?: string;
        work_date?: string;
        work_in?: string;
        work_out?: string;
        work_hour?: number;
        salary?: number;
        id: number;
    } & import("./entities/salary.entity").Salary>;
    remove(id: string): Promise<import("./entities/salary.entity").Salary>;
}
