export declare class Salary {
    id: number;
    name: string;
    position: string;
    work_date: string;
    work_in: string;
    work_out: string;
    work_hour: number;
    salary: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
