"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalaryService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const salary_entity_1 = require("./entities/salary.entity");
let SalaryService = class SalaryService {
    constructor(salaryRepository) {
        this.salaryRepository = salaryRepository;
    }
    create(createSalaryDto) {
        return this.salaryRepository.save(createSalaryDto);
    }
    findAll() {
        return this.salaryRepository.find();
    }
    findOne(id) {
        return this.salaryRepository.findOne({ where: { id: id } });
    }
    async update(id, updateSalaryDto) {
        try {
            const updatedSalary = await this.salaryRepository.save(Object.assign({ id }, updateSalaryDto));
            return updatedSalary;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
    async remove(id) {
        const salary = await this.salaryRepository.findOne({
            where: { id: id },
        });
        try {
            const deletedSalary = await this.salaryRepository.remove(salary);
            return deletedSalary;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
};
SalaryService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(salary_entity_1.Salary)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SalaryService);
exports.SalaryService = SalaryService;
//# sourceMappingURL=salary.service.js.map