import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
export declare class SalaryService {
    private salaryRepository;
    constructor(salaryRepository: Repository<Salary>);
    create(createSalaryDto: CreateSalaryDto): Promise<CreateSalaryDto & Salary>;
    findAll(): Promise<Salary[]>;
    findOne(id: number): Promise<Salary>;
    update(id: number, updateSalaryDto: UpdateSalaryDto): Promise<{
        name?: string;
        position?: string;
        work_date?: string;
        work_in?: string;
        work_out?: string;
        work_hour?: number;
        salary?: number;
        id: number;
    } & Salary>;
    remove(id: number): Promise<Salary>;
}
