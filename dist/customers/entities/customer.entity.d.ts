import { Order } from 'src/orders/entities/order.entity';
export declare class Customer {
    id: number;
    name: string;
    age: number;
    tel: string;
    gender: string;
    orders: Order[];
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
}
