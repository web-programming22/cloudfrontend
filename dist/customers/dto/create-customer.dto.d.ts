export declare class CreateCustomerDto {
    name: string;
    age: number;
    tel: string;
    gender: string;
}
