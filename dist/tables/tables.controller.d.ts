import { TablesService } from './tables.service';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
export declare class TablesController {
    private readonly tablesService;
    constructor(tablesService: TablesService);
    create(createTableDto: CreateTableDto): Promise<CreateTableDto & import("./entities/table.entity").Table>;
    findAll(): Promise<import("./entities/table.entity").Table[]>;
    findOne(id: string): Promise<import("./entities/table.entity").Table>;
    update(id: string, updateTableDto: UpdateTableDto): Promise<{
        table: string;
        seat: number;
        status: string;
        id: number;
        createdAt: Date;
        updatedAt: Date;
        deletedAt: Date;
        orders: import("../orders/entities/order.entity").Order[];
    } & import("./entities/table.entity").Table>;
    remove(id: string): Promise<import("./entities/table.entity").Table>;
    findStatusTable(status: string): Promise<import("./entities/table.entity").Table[]>;
}
