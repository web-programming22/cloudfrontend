import { Order } from 'src/orders/entities/order.entity';
export declare class Table {
    id: number;
    table: string;
    seat: number;
    status: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    orders: Order[];
}
