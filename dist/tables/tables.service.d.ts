import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';
export declare class TablesService {
    private tablesRepository;
    constructor(tablesRepository: Repository<Table>);
    create(createTableDto: CreateTableDto): Promise<CreateTableDto & Table>;
    findAll(): Promise<Table[]>;
    findStatusTable(status: string): Promise<Table[]>;
    findOne(id: number): Promise<Table>;
    update(id: number, updateTableDto: UpdateTableDto): Promise<{
        table: string;
        seat: number;
        status: string;
        id: number;
        createdAt: Date;
        updatedAt: Date;
        deletedAt: Date;
        orders: import("../orders/entities/order.entity").Order[];
    } & Table>;
    remove(id: number): Promise<Table>;
}
