export declare class CreateTableDto {
    table: string;
    seat: number;
    status: string;
}
