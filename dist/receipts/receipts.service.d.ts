import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { ReceiptItem } from './entities/receipt-items';
import { Receipt } from './entities/receipt.entity';
export declare class ReceiptsService {
    private receiptRepository;
    private materialRepository;
    private receiptItemRepository;
    constructor(receiptRepository: Repository<Receipt>, materialRepository: Repository<Material>, receiptItemRepository: Repository<ReceiptItem>);
    create(createReceiptDto: CreateReceiptDto): Promise<Receipt>;
    findAll(): Promise<Receipt[]>;
    findOne(id: number): Promise<Receipt>;
    update(id: number, updateReceiptDto: UpdateReceiptDto): string;
    remove(id: number): Promise<Receipt>;
}
