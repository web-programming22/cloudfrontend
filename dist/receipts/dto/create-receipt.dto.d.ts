export declare class CreateReceiptDto {
    total: number;
    receive: number;
    change: number;
    nameEmp: string;
    receiptItems: CreatedReceiptItemDto[];
}
export declare class CreatedReceiptItemDto {
    name: string;
    amount: number;
    price: number;
    total: number;
    receiptId: number;
    materialId: number;
}
