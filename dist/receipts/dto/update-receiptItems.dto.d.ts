import { CreatedReceiptItemDto } from './create-receipt.dto';
declare const UpdateReceiptItemDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreatedReceiptItemDto>>;
export declare class UpdateReceiptItemDto extends UpdateReceiptItemDto_base {
}
export {};
