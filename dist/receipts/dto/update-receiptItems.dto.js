"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateReceiptItemDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_receipt_dto_1 = require("./create-receipt.dto");
class UpdateReceiptItemDto extends (0, mapped_types_1.PartialType)(create_receipt_dto_1.CreatedReceiptItemDto) {
}
exports.UpdateReceiptItemDto = UpdateReceiptItemDto;
//# sourceMappingURL=update-receiptItems.dto.js.map