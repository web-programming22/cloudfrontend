import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreatedReceiptItemDto } from './dto/create-receipt.dto';
import { ReceiptItem } from './entities/receipt-items';
import { Receipt } from './entities/receipt.entity';
import { UpdateReceiptItemDto } from './dto/update-receiptItems.dto';
export declare class ReceiptsItemsService {
    private receiptRepository;
    private materialRepository;
    private receiptItemRepository;
    constructor(receiptRepository: Repository<Receipt>, materialRepository: Repository<Material>, receiptItemRepository: Repository<ReceiptItem>);
    create(createReceiptItemsDto: CreatedReceiptItemDto): Promise<void>;
    findAll(): Promise<ReceiptItem[]>;
    update(id: number, updateReceiptItemsDto: UpdateReceiptItemDto): Promise<UpdateReceiptItemDto>;
    remove(id: number): Promise<ReceiptItem>;
}
