import { Material } from 'src/material/entities/material.entity';
import { Receipt } from './receipt.entity';
export declare class ReceiptItem {
    id: number;
    name: string;
    amount: number;
    price: number;
    total: number;
    createdDate: Date;
    updateDate: Date;
    deleteDate: Date;
    receipt: Receipt;
    material: Material;
}
