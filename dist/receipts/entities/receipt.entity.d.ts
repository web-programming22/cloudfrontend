import { ReceiptItem } from './receipt-items';
export declare class Receipt {
    id: number;
    total: number;
    receive: number;
    change: number;
    nameEmp: string;
    createdDate: Date;
    updateDate: Date;
    deleteDate: Date;
    receiptItems: ReceiptItem[];
}
