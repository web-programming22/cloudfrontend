"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const material_entity_1 = require("../material/entities/material.entity");
const typeorm_2 = require("typeorm");
const receipt_items_1 = require("./entities/receipt-items");
const receipt_entity_1 = require("./entities/receipt.entity");
let ReceiptsService = class ReceiptsService {
    constructor(receiptRepository, materialRepository, receiptItemRepository) {
        this.receiptRepository = receiptRepository;
        this.materialRepository = materialRepository;
        this.receiptItemRepository = receiptItemRepository;
    }
    async create(createReceiptDto) {
        const receipt = new receipt_entity_1.Receipt();
        receipt.nameEmp = createReceiptDto.nameEmp;
        receipt.total = createReceiptDto.total;
        receipt.receive = createReceiptDto.receive;
        receipt.change = createReceiptDto.change;
        await this.receiptRepository.save(receipt);
        for (const re of createReceiptDto.receiptItems) {
            const receiptItem = new receipt_items_1.ReceiptItem();
            receiptItem.name = re.name;
            receiptItem.amount = re.amount;
            receiptItem.price = re.price;
            receiptItem.total = re.total;
            receiptItem.material = await this.materialRepository.findOneBy({
                id: re.materialId,
            });
            receiptItem.receipt = await this.receiptRepository.findOneBy({
                id: re.receiptId,
            });
            receiptItem.receipt = receipt;
            await this.receiptItemRepository.save(receiptItem);
        }
        await this.receiptRepository.save(receipt);
        return this.receiptRepository.findOne({
            where: { id: receipt.id },
            relations: ['receiptItems'],
        });
    }
    findAll() {
        return this.receiptRepository.find({ relations: ['receiptItems'] });
    }
    findOne(id) {
        return this.receiptRepository.findOne({
            where: { id: id },
            relations: ['receiptItems', 'material'],
        });
    }
    update(id, updateReceiptDto) {
        return `This action updates a #${id} receipt`;
    }
    async remove(id) {
        const receipt = await this.receiptRepository.findOneBy({ id: id });
        return this.receiptRepository.softRemove(receipt);
    }
};
ReceiptsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(receipt_entity_1.Receipt)),
    __param(1, (0, typeorm_1.InjectRepository)(material_entity_1.Material)),
    __param(2, (0, typeorm_1.InjectRepository)(receipt_items_1.ReceiptItem)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ReceiptsService);
exports.ReceiptsService = ReceiptsService;
//# sourceMappingURL=receipts.service.js.map