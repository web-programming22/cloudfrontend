"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsItemsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const material_entity_1 = require("../material/entities/material.entity");
const typeorm_2 = require("typeorm");
const receipt_items_1 = require("./entities/receipt-items");
const receipt_entity_1 = require("./entities/receipt.entity");
let ReceiptsItemsService = class ReceiptsItemsService {
    constructor(receiptRepository, materialRepository, receiptItemRepository) {
        this.receiptRepository = receiptRepository;
        this.materialRepository = materialRepository;
        this.receiptItemRepository = receiptItemRepository;
    }
    async create(createReceiptItemsDto) {
    }
    findAll() {
        return this.receiptItemRepository.find({ relations: ['material'] });
    }
    async update(id, updateReceiptItemsDto) {
        try {
            const updatedRecriptItem = await this.receiptItemRepository.save(Object.assign({ id }, updateReceiptItemsDto));
            return updateReceiptItemsDto;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
    async remove(id) {
        const receiptItem = await this.receiptItemRepository.findOneBy({ id: id });
        return this.receiptItemRepository.softRemove(receiptItem);
    }
};
ReceiptsItemsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(receipt_entity_1.Receipt)),
    __param(1, (0, typeorm_1.InjectRepository)(material_entity_1.Material)),
    __param(2, (0, typeorm_1.InjectRepository)(receipt_items_1.ReceiptItem)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ReceiptsItemsService);
exports.ReceiptsItemsService = ReceiptsItemsService;
//# sourceMappingURL=receiptsItem.service.js.map