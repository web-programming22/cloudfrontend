import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { ReceiptsItemsService } from './receiptsItem.service';
export declare class ReceiptsItemsController {
    private readonly receiptsItemsService;
    constructor(receiptsItemsService: ReceiptsItemsService);
    findAll(): Promise<import("./entities/receipt-items").ReceiptItem[]>;
    update(id: string, updateReceiptItemDto: UpdateReceiptDto): Promise<import("./dto/update-receiptItems.dto").UpdateReceiptItemDto>;
    remove(id: string): Promise<import("./entities/receipt-items").ReceiptItem>;
}
