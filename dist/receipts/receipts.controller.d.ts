import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
export declare class ReceiptsController {
    private readonly receiptsService;
    constructor(receiptsService: ReceiptsService);
    create(createReceiptDto: CreateReceiptDto): Promise<import("./entities/receipt.entity").Receipt>;
    findAll(): Promise<import("./entities/receipt.entity").Receipt[]>;
    findOne(id: string): Promise<import("./entities/receipt.entity").Receipt>;
    update(id: string, updateReceiptDto: UpdateReceiptDto): string;
    remove(id: string): Promise<import("./entities/receipt.entity").Receipt>;
}
