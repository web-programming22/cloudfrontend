export declare class Emp {
    id: number;
    name: string;
    gender: string;
    position: string;
    tel: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
