export declare class CreateEmpDto {
    name: string;
    gender: string;
    position: string;
    tel: string;
}
