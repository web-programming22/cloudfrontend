"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateEmpDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_emp_dto_1 = require("./create-emp.dto");
class UpdateEmpDto extends (0, mapped_types_1.PartialType)(create_emp_dto_1.CreateEmpDto) {
}
exports.UpdateEmpDto = UpdateEmpDto;
//# sourceMappingURL=update-emp.dto.js.map