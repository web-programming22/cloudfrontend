import { EmpService } from './emp.service';
import { CreateEmpDto } from './dto/create-emp.dto';
import { UpdateEmpDto } from './dto/update-emp.dto';
export declare class EmpController {
    private readonly empService;
    constructor(empService: EmpService);
    create(createEmpDto: CreateEmpDto): Promise<CreateEmpDto & import("./entities/emp.entity").Emp>;
    findAll(): Promise<import("./entities/emp.entity").Emp[]>;
    findOne(id: string): Promise<import("./entities/emp.entity").Emp>;
    update(id: string, updateEmpDto: UpdateEmpDto): Promise<{
        name?: string;
        gender?: string;
        position?: string;
        tel?: string;
        id: number;
    } & import("./entities/emp.entity").Emp>;
    remove(id: string): Promise<import("./entities/emp.entity").Emp>;
}
