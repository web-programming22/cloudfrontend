"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmpController = void 0;
const common_1 = require("@nestjs/common");
const emp_service_1 = require("./emp.service");
const create_emp_dto_1 = require("./dto/create-emp.dto");
const update_emp_dto_1 = require("./dto/update-emp.dto");
let EmpController = class EmpController {
    constructor(empService) {
        this.empService = empService;
    }
    create(createEmpDto) {
        return this.empService.create(createEmpDto);
    }
    findAll() {
        return this.empService.findAll();
    }
    findOne(id) {
        return this.empService.findOne(+id);
    }
    update(id, updateEmpDto) {
        return this.empService.update(+id, updateEmpDto);
    }
    remove(id) {
        return this.empService.remove(+id);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_emp_dto_1.CreateEmpDto]),
    __metadata("design:returntype", void 0)
], EmpController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], EmpController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EmpController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_emp_dto_1.UpdateEmpDto]),
    __metadata("design:returntype", void 0)
], EmpController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EmpController.prototype, "remove", null);
EmpController = __decorate([
    (0, common_1.Controller)('Emp'),
    __metadata("design:paramtypes", [emp_service_1.EmpService])
], EmpController);
exports.EmpController = EmpController;
//# sourceMappingURL=emp.controller.js.map