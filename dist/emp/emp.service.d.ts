import { Repository } from 'typeorm';
import { CreateEmpDto } from './dto/create-emp.dto';
import { UpdateEmpDto } from './dto/update-emp.dto';
import { Emp } from './entities/emp.entity';
export declare class EmpService {
    private empRepository;
    constructor(empRepository: Repository<Emp>);
    create(createEmpDto: CreateEmpDto): Promise<CreateEmpDto & Emp>;
    findAll(): Promise<Emp[]>;
    findOne(id: number): Promise<Emp>;
    update(id: number, updateEmpDto: UpdateEmpDto): Promise<{
        name?: string;
        gender?: string;
        position?: string;
        tel?: string;
        id: number;
    } & Emp>;
    remove(id: number): Promise<Emp>;
}
