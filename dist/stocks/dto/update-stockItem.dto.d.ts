import { CreateStockItemDto } from './create-stock.dto';
declare const UpdateStockItemsDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateStockItemDto>>;
export declare class UpdateStockItemsDto extends UpdateStockItemsDto_base {
}
export {};
