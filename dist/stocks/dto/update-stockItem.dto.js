"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateStockItemsDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const create_stock_dto_1 = require("./create-stock.dto");
class UpdateStockItemsDto extends (0, mapped_types_1.PartialType)(create_stock_dto_1.CreateStockItemDto) {
}
exports.UpdateStockItemsDto = UpdateStockItemsDto;
//# sourceMappingURL=update-stockItem.dto.js.map