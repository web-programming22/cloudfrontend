export declare class CreateStockDto {
    nameEmp: string;
}
export declare class CreateStockItemDto {
    StockId: number;
    MaterialId: number;
    amount: number;
    price: number;
    nameMaterial: string;
}
