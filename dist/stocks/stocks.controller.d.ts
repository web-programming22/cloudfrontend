import { StocksService } from './stocks.service';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
export declare class StocksController {
    private readonly stocksService;
    constructor(stocksService: StocksService);
    create(createStockDto: CreateStockDto): Promise<CreateStockDto & import("./entities/stock.entity").Stock>;
    findAll(): Promise<import("./entities/stock.entity").Stock[]>;
    findOne(id: string): Promise<import("./entities/stock.entity").Stock>;
    update(id: string, updateStockDto: UpdateStockDto): string;
    remove(id: string): Promise<import("./entities/stock.entity").Stock>;
}
