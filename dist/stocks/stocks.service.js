"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StocksService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const material_entity_1 = require("../material/entities/material.entity");
const typeorm_2 = require("typeorm");
const stock_items_1 = require("./entities/stock-items");
const stock_entity_1 = require("./entities/stock.entity");
let StocksService = class StocksService {
    constructor(stockRepository, materialRepository, stockItemRepository) {
        this.stockRepository = stockRepository;
        this.materialRepository = materialRepository;
        this.stockItemRepository = stockItemRepository;
    }
    async create(createStockDto) {
        return this.stockRepository.save(createStockDto);
    }
    findAll() {
        return this.stockRepository.find({ relations: ['stockItem'] });
    }
    findOne(id) {
        return this.stockRepository.findOne({
            where: { id: id },
            relations: ['stockItems'],
        });
    }
    update(id, updateStockDto) {
        return `This action updates a #${id} stock`;
    }
    async remove(id) {
        const stock = await this.stockRepository.findOneBy({ id: id });
        return this.stockRepository.softRemove(stock);
    }
};
StocksService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(stock_entity_1.Stock)),
    __param(1, (0, typeorm_1.InjectRepository)(material_entity_1.Material)),
    __param(2, (0, typeorm_1.InjectRepository)(stock_items_1.StockItem)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], StocksService);
exports.StocksService = StocksService;
//# sourceMappingURL=stocks.service.js.map