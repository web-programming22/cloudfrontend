import { CreateStockItemDto } from './dto/create-stock.dto';
import { StocksItemsService } from './stocksItems.service';
import { UpdateStockItemsDto } from './dto/update-stockItem.dto';
export declare class StocksItemsController {
    private readonly stocksItemsService;
    constructor(stocksItemsService: StocksItemsService);
    create(createItemsStockDto: CreateStockItemDto): Promise<CreateStockItemDto & import("./entities/stock-items").StockItem>;
    findAll(): Promise<import("./entities/stock-items").StockItem[]>;
    update(id: string, updateStockItemsDto: UpdateStockItemsDto): Promise<UpdateStockItemsDto>;
    remove(id: string): Promise<import("./entities/stock-items").StockItem>;
}
