import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { StockItem } from './entities/stock-items';
import { Stock } from './entities/stock.entity';
export declare class StocksService {
    private stockRepository;
    private materialRepository;
    private stockItemRepository;
    constructor(stockRepository: Repository<Stock>, materialRepository: Repository<Material>, stockItemRepository: Repository<StockItem>);
    create(createStockDto: CreateStockDto): Promise<CreateStockDto & Stock>;
    findAll(): Promise<Stock[]>;
    findOne(id: number): Promise<Stock>;
    update(id: number, updateStockDto: UpdateStockDto): string;
    remove(id: number): Promise<Stock>;
}
