import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import { CreateStockItemDto } from './dto/create-stock.dto';
import { StockItem } from './entities/stock-items';
import { Stock } from './entities/stock.entity';
import { UpdateStockItemsDto } from './dto/update-stockItem.dto';
export declare class StocksItemsService {
    private stockRepository;
    private materialRepository;
    private stockItemRepository;
    constructor(stockRepository: Repository<Stock>, materialRepository: Repository<Material>, stockItemRepository: Repository<StockItem>);
    create(createStockItemsDto: CreateStockItemDto): Promise<CreateStockItemDto & StockItem>;
    findAll(): Promise<StockItem[]>;
    update(id: number, updateStockItemsDto: UpdateStockItemsDto): Promise<UpdateStockItemsDto>;
    remove(id: number): Promise<StockItem>;
}
