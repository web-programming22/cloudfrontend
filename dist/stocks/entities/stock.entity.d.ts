import { StockItem } from './stock-items';
export declare class Stock {
    id: number;
    nameEmp: string;
    createdDate: Date;
    updateDate: Date;
    deleteDate: Date;
    stockItem: StockItem[];
}
