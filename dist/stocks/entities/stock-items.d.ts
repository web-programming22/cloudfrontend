import { Material } from 'src/material/entities/material.entity';
import { Stock } from './stock.entity';
export declare class StockItem {
    id: number;
    nameMaterial: string;
    amount: number;
    price: number;
    createdDate: Date;
    updateDate: Date;
    deleteDate: Date;
    stock: Stock;
    material: Material;
}
