"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StocksItemsController = void 0;
const common_1 = require("@nestjs/common");
const create_stock_dto_1 = require("./dto/create-stock.dto");
const stocksItems_service_1 = require("./stocksItems.service");
const update_stockItem_dto_1 = require("./dto/update-stockItem.dto");
let StocksItemsController = class StocksItemsController {
    constructor(stocksItemsService) {
        this.stocksItemsService = stocksItemsService;
    }
    create(createItemsStockDto) {
        return this.stocksItemsService.create(createItemsStockDto);
    }
    findAll() {
        return this.stocksItemsService.findAll();
    }
    update(id, updateStockItemsDto) {
        return this.stocksItemsService.update(+id, updateStockItemsDto);
    }
    remove(id) {
        return this.stocksItemsService.remove(+id);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_stock_dto_1.CreateStockItemDto]),
    __metadata("design:returntype", void 0)
], StocksItemsController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], StocksItemsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_stockItem_dto_1.UpdateStockItemsDto]),
    __metadata("design:returntype", void 0)
], StocksItemsController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StocksItemsController.prototype, "remove", null);
StocksItemsController = __decorate([
    (0, common_1.Controller)('stocksItems'),
    __metadata("design:paramtypes", [stocksItems_service_1.StocksItemsService])
], StocksItemsController);
exports.StocksItemsController = StocksItemsController;
//# sourceMappingURL=stocksItems.controller.js.map