import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
export declare class AppController {
    private readonly appService;
    private authService;
    constructor(appService: AppService, authService: AuthService);
    login(req: any): Promise<{
        user: import("./users/entities/user.entity").User;
        access_token: string;
    }>;
    getHello(): string;
}
