"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const customer_entity_1 = require("../customers/entities/customer.entity");
const food_entity_1 = require("../foods/entities/food.entity");
const table_entity_1 = require("../tables/entities/table.entity");
const typeorm_2 = require("typeorm");
const order_item_1 = require("./entities/order-item");
const order_entity_1 = require("./entities/order.entity");
let OrdersService = class OrdersService {
    constructor(ordersRepository, customersRepository, foodsRepository, orderItemsRepository, tablesRepository) {
        this.ordersRepository = ordersRepository;
        this.customersRepository = customersRepository;
        this.foodsRepository = foodsRepository;
        this.orderItemsRepository = orderItemsRepository;
        this.tablesRepository = tablesRepository;
    }
    async create(createOrderDto) {
        console.log(createOrderDto);
        const customer = await this.customersRepository.findOneBy({
            id: createOrderDto.customerId,
        });
        const table = await this.tablesRepository.findOneBy({
            id: createOrderDto.tableId,
        });
        const order = new order_entity_1.Order();
        order.customer = customer;
        order.table = table;
        order.amount = 0;
        order.total = 0;
        await this.ordersRepository.save(order);
        for (const od of createOrderDto.orderItems) {
            const orderItem = new order_item_1.OrderItem();
            orderItem.amount = od.amount;
            orderItem.food = await this.foodsRepository.findOneBy({
                id: od.foodId,
            });
            orderItem.name = orderItem.food.name;
            orderItem.price = orderItem.food.price;
            orderItem.total = orderItem.price * orderItem.amount;
            orderItem.order = order;
            await this.orderItemsRepository.save(orderItem);
            order.amount = order.amount + orderItem.amount;
            order.total = order.total + orderItem.total;
        }
        await this.ordersRepository.save(order);
        return await this.ordersRepository.findOne({
            where: { id: order.id },
            relations: ['orderItems'],
        });
    }
    findAll() {
        return this.ordersRepository.find({
            relations: ['customer', 'orderItems'],
        });
    }
    findOne(id) {
        return this.ordersRepository.findOne({
            where: { id: id },
            relations: ['table', 'customer', 'orderItems'],
        });
    }
    findOrderTableId(tableId) {
        return this.ordersRepository.find({
            where: { tableId: tableId },
            relations: ['orderItems'],
        });
    }
    async update(id, updateOrderDto) {
        const order = await this.ordersRepository.findOneBy({ id: id });
        try {
            const updatedOrder = await this.ordersRepository.save(Object.assign({ id }, updateOrderDto));
            for (const od of updateOrderDto.orderItems) {
                const orderItem = new order_item_1.OrderItem();
                orderItem.amount = od.amount;
                orderItem.food = await this.foodsRepository.findOneBy({
                    id: od.foodId,
                });
                orderItem.name = orderItem.food.name;
                orderItem.price = orderItem.food.price;
                orderItem.total = orderItem.price * orderItem.amount;
                orderItem.order = order;
                await this.orderItemsRepository.save(orderItem);
            }
            return updatedOrder;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
    async remove(id) {
        const order = await this.ordersRepository.findOneBy({ id: id });
        return this.ordersRepository.softRemove(order);
    }
};
OrdersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(order_entity_1.Order)),
    __param(1, (0, typeorm_1.InjectRepository)(customer_entity_1.Customer)),
    __param(2, (0, typeorm_1.InjectRepository)(food_entity_1.Food)),
    __param(3, (0, typeorm_1.InjectRepository)(order_item_1.OrderItem)),
    __param(4, (0, typeorm_1.InjectRepository)(table_entity_1.Table)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], OrdersService);
exports.OrdersService = OrdersService;
//# sourceMappingURL=orders.service.js.map