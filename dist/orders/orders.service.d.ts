import { Customer } from 'src/customers/entities/customer.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
export declare class OrdersService {
    private ordersRepository;
    private customersRepository;
    private foodsRepository;
    private orderItemsRepository;
    private tablesRepository;
    constructor(ordersRepository: Repository<Order>, customersRepository: Repository<Customer>, foodsRepository: Repository<Food>, orderItemsRepository: Repository<OrderItem>, tablesRepository: Repository<Table>);
    create(createOrderDto: CreateOrderDto): Promise<Order>;
    findAll(): Promise<Order[]>;
    findOne(id: number): Promise<Order>;
    findOrderTableId(tableId: number): Promise<Order[]>;
    update(id: number, updateOrderDto: UpdateOrderDto): Promise<{
        customerId?: number;
        tableId?: number;
        orderItems?: {
            foodId: number;
            amount: number;
        }[];
        id: number;
    } & Order>;
    remove(id: number): Promise<Order>;
}
