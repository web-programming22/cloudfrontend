import { Food } from 'src/foods/entities/food.entity';
import { Quesfood } from 'src/quesfoods/entities/quesfood.entity';
import { Order } from './order.entity';
export declare class OrderItem {
    id: number;
    name: string;
    price: number;
    amount: number;
    total: number;
    order: Order;
    food: Food;
    quefood: Quesfood[];
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
}
