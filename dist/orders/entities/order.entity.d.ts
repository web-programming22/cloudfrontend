import { Customer } from 'src/customers/entities/customer.entity';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from './order-item';
export declare class Order {
    id: number;
    amount: number;
    total: number;
    customer: Customer;
    createdDate: Date;
    updatedDate: Date;
    deletedDate: Date;
    orderItems: OrderItem[];
    table: Table;
    tableId: number;
}
