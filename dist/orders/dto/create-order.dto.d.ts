declare class CreatedOrderItemDto {
    foodId: number;
    amount: number;
}
export declare class CreateOrderDto {
    customerId: number;
    tableId: number;
    orderItems: CreatedOrderItemDto[];
}
export {};
