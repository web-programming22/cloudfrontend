export declare class CreateFoodDto {
    name: string;
    price: number;
    image: string;
    categoryId: number;
}
