/// <reference types="multer" />
import { FoodsService } from './foods.service';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Response } from 'express';
export declare class FoodsController {
    private readonly foodsService;
    constructor(foodsService: FoodsService);
    create(createFoodDto: CreateFoodDto, file: Express.Multer.File): Promise<import("./entities/food.entity").Food>;
    findByCategory(id: string): Promise<import("./entities/food.entity").Food[]>;
    findAll(query: {
        cat?: string;
        order?: string;
        orderBy?: string;
    }): Promise<import("./entities/food.entity").Food[]>;
    findOne(id: string): Promise<import("./entities/food.entity").Food>;
    getImage(id: string, res: Response): Promise<void>;
    getImageByFileName(imageFile: string, res: Response): Promise<void>;
    updateImage(id: string, file: Express.Multer.File): Promise<{
        name?: string;
        price?: number;
        image?: string;
        categoryId?: number;
        id: number;
    } & import("./entities/food.entity").Food>;
    update(id: string, updateFoodDto: UpdateFoodDto, file: Express.Multer.File): Promise<{
        name?: string;
        price?: number;
        image?: string;
        categoryId?: number;
        id: number;
    } & import("./entities/food.entity").Food>;
    remove(id: string): Promise<import("./entities/food.entity").Food>;
}
