import { Category } from 'src/categories/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';
export declare class FoodsService {
    private foodsRepository;
    private categoriesRepository;
    constructor(foodsRepository: Repository<Food>, categoriesRepository: Repository<Category>);
    create(createFoodDto: CreateFoodDto): Promise<Food>;
    findByCategory(id: number): Promise<Food[]>;
    findAll(option: any): Promise<Food[]>;
    findOne(id: number): Promise<Food>;
    update(id: number, updateFoodDto: UpdateFoodDto): Promise<{
        name?: string;
        price?: number;
        image?: string;
        categoryId?: number;
        id: number;
    } & Food>;
    remove(id: number): Promise<Food>;
}
