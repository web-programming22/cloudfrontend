"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Food = void 0;
const category_entity_1 = require("../../categories/entities/category.entity");
const order_item_1 = require("../../orders/entities/order-item");
const typeorm_1 = require("typeorm");
let Food = class Food {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Food.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], Food.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float' }),
    __metadata("design:type", Number)
], Food.prototype, "price", void 0);
__decorate([
    (0, typeorm_1.Column)({
        length: '128',
        default: 'no_img_available.jpg',
    }),
    __metadata("design:type", String)
], Food.prototype, "image", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Food.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Food.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.DeleteDateColumn)(),
    __metadata("design:type", Date)
], Food.prototype, "deletedAt", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => order_item_1.OrderItem, (orderItem) => orderItem.food),
    __metadata("design:type", Array)
], Food.prototype, "orderItems", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => category_entity_1.Category, (category) => category.foods),
    __metadata("design:type", category_entity_1.Category)
], Food.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], Food.prototype, "categoryId", void 0);
Food = __decorate([
    (0, typeorm_1.Entity)()
], Food);
exports.Food = Food;
//# sourceMappingURL=food.entity.js.map