import { Category } from 'src/categories/entities/category.entity';
import { OrderItem } from 'src/orders/entities/order-item';
export declare class Food {
    id: number;
    name: string;
    price: number;
    image: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    orderItems: OrderItem[];
    category: Category;
    categoryId: number;
}
