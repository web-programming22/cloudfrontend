"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const category_entity_1 = require("../categories/entities/category.entity");
const typeorm_2 = require("typeorm");
const food_entity_1 = require("./entities/food.entity");
let FoodsService = class FoodsService {
    constructor(foodsRepository, categoriesRepository) {
        this.foodsRepository = foodsRepository;
        this.categoriesRepository = categoriesRepository;
    }
    async create(createFoodDto) {
        const category = await this.categoriesRepository.findOne({
            where: {
                id: createFoodDto.categoryId,
            },
        });
        const newFood = new food_entity_1.Food();
        newFood.name = createFoodDto.name;
        newFood.price = createFoodDto.price;
        newFood.image = createFoodDto.image;
        newFood.category = category;
        return this.foodsRepository.save(newFood);
    }
    findByCategory(id) {
        return this.foodsRepository.find({ where: { categoryId: id } });
    }
    findAll(option) {
        return this.foodsRepository.find(option);
    }
    findOne(id) {
        return this.foodsRepository.findOne({ where: { id: id } });
    }
    async update(id, updateFoodDto) {
        try {
            const updatedFood = await this.foodsRepository.save(Object.assign({ id }, updateFoodDto));
            return updatedFood;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
    async remove(id) {
        const food = await this.foodsRepository.findOne({
            where: { id: id },
        });
        try {
            const deletedFood = await this.foodsRepository.softRemove(food);
            return deletedFood;
        }
        catch (e) {
            throw new common_1.NotFoundException();
        }
    }
};
FoodsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(food_entity_1.Food)),
    __param(1, (0, typeorm_1.InjectRepository)(category_entity_1.Category)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], FoodsService);
exports.FoodsService = FoodsService;
//# sourceMappingURL=foods.service.js.map