"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FoodsModule = void 0;
const common_1 = require("@nestjs/common");
const foods_service_1 = require("./foods.service");
const foods_controller_1 = require("./foods.controller");
const dist_1 = require("@nestjs/typeorm/dist");
const food_entity_1 = require("./entities/food.entity");
const category_entity_1 = require("../categories/entities/category.entity");
let FoodsModule = class FoodsModule {
};
FoodsModule = __decorate([
    (0, common_1.Module)({
        imports: [dist_1.TypeOrmModule.forFeature([food_entity_1.Food, category_entity_1.Category])],
        controllers: [foods_controller_1.FoodsController],
        providers: [foods_service_1.FoodsService],
    })
], FoodsModule);
exports.FoodsModule = FoodsModule;
//# sourceMappingURL=foods.module.js.map