import { IsNotEmpty } from 'class-validator';
import { OrderItem } from 'src/orders/entities/order-item';
export class CreateQuesfoodDto {
  @IsNotEmpty()
  status: string;

  @IsNotEmpty()
  orderItemId: number;
}
