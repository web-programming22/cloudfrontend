import { Test, TestingModule } from '@nestjs/testing';
import { QuesfoodsService } from './quesfoods.service';

describe('QuesfoodsService', () => {
  let service: QuesfoodsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuesfoodsService],
    }).compile();

    service = module.get<QuesfoodsService>(QuesfoodsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
