import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NotFoundError } from 'rxjs';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}

  create(createTableDto: CreateTableDto) {
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find({});
  }

  findStatusTable(status: string) {
    return this.tablesRepository.find({ where: { status: status } });
  }

  async findOne(id: number) {
    const table = await this.tablesRepository.findOne({ where: { id: id } });
    if (!table) {
      throw new NotFoundException();
    }
    return table;
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    const updatedTable = { ...table, ...updateTableDto };
    return this.tablesRepository.save(updatedTable);
  }

  async remove(id: number) {
    const table = await this.tablesRepository.findOne({ where: { id: id } });
    try {
      const deletedTable = await this.tablesRepository.softRemove(table);
      return deletedTable;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
