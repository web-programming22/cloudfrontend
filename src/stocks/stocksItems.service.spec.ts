import { Test, TestingModule } from '@nestjs/testing';
import { StocksService } from './stocks.service';
import { StocksItemsService } from './stocksItems.service';

describe('StocksService', () => {
  let service: StocksItemsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StocksItemsService],
    }).compile();

    service = module.get<StocksItemsService>(StocksItemsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
