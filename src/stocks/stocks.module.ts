import { Module } from '@nestjs/common';
import { StocksService } from './stocks.service';
import { StocksController } from './stocks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Material } from 'src/material/entities/material.entity';
import { StockItem } from './entities/stock-items';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, StockItem, Material])],
  controllers: [StocksController],
  providers: [StocksService],
})
export class StocksModule {}
