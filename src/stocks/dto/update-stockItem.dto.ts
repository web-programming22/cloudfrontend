import { PartialType } from '@nestjs/mapped-types';
import { CreateStockDto, CreateStockItemDto } from './create-stock.dto';

export class UpdateStockItemsDto extends PartialType(CreateStockItemDto) {}
