import { IsPositive, IsNotEmpty, IsEmpty } from 'class-validator';
export class CreateStockDto {
  @IsNotEmpty()
  nameEmp: string;
  // @IsNotEmpty()
  // stockItem: CreateStockItemDto[];
}

export class CreateStockItemDto {
  @IsNotEmpty()
  StockId: number;
  @IsNotEmpty()
  MaterialId: number;
  @IsNotEmpty()
  amount: number;
  @IsNotEmpty()
  price: number;
  @IsNotEmpty()
  nameMaterial: string;
}
