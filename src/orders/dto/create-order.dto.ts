class CreatedOrderItemDto {
  foodId: number;
  amount: number;
}
export class CreateOrderDto {
  customerId: number;
  tableId: number;
  orderItems: CreatedOrderItemDto[];
}
