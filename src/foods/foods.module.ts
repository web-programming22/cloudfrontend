import { Module } from '@nestjs/common';
import { FoodsService } from './foods.service';
import { FoodsController } from './foods.controller';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Food } from './entities/food.entity';
import { Category } from 'src/categories/entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Food, Category])], // อะไรที่ต้องใช้มาใส่ในนี้ Food
  controllers: [FoodsController],
  providers: [FoodsService],
})
export class FoodsModule {}
