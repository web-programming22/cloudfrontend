import { IsNotEmpty, Length, IsInt, Max } from 'class-validator';

export class CreateSalaryDto {
  @IsNotEmpty()
  @Length(3, 30)
  name: string;

  @IsNotEmpty()
  @Length(3, 30)
  position: string;

  @IsNotEmpty()
  work_date: string;

  @IsNotEmpty()
  work_in: string;

  @IsNotEmpty()
  work_out: string;

  @IsNotEmpty()
  @Max(10)
  @IsInt()
  work_hour: number;

  @IsNotEmpty()
  @IsInt()
  salary: number;
}
