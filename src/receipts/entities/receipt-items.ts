import { Material } from 'src/material/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Receipt } from './receipt.entity';
@Entity()
export class ReceiptItem {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  amount: number;
  @Column({ type: 'double' })
  price: number;
  @Column({ type: 'double' })
  total: number;
  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updateDate: Date;
  @DeleteDateColumn()
  deleteDate: Date;
  @ManyToOne(() => Receipt, (receipt) => receipt.receiptItems)
  receipt: Receipt;
  @ManyToOne(() => Material, (material) => material.receiptItems)
  material: Material;
}
