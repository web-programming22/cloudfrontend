import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from 'src/material/entities/material.entity';
import { Repository } from 'typeorm';
import {
  CreateReceiptDto,
  CreatedReceiptItemDto,
} from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { ReceiptItem } from './entities/receipt-items';
import { Receipt } from './entities/receipt.entity';
import { UpdateReceiptItemDto } from './dto/update-receiptItems.dto';

@Injectable()
export class ReceiptsItemsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(ReceiptItem)
    private receiptItemRepository: Repository<ReceiptItem>,
  ) {}
  async create(createReceiptItemsDto: CreatedReceiptItemDto) {
    // console.log(createReceiptItemsDto);
    // const receipt: Receipt = new Receipt();
    // //เริ่มต้น
    // receipt.nameEmp = 'องอาจ';
    // receipt.total = 0;
    // receipt.receive = 5000; // จ่าย
    // receipt.change = 0;
    // await this.receiptRepository.save(receipt); // ได้ id receipt
    // for (const re of createReceiptDto.receiptItem) {
    //   const receiptItem = new ReceiptItem();
    //   receiptItem.amount = re.amount;
    //   receiptItem.price = re.price;
    //   receiptItem.total = re.total;
    //   receiptItem.material = await this.materialRepository.findOneBy({
    //     id: re.materialId,
    //   }); //FK
    //   receiptItem.name = receiptItem.material.name; // ชื่อ material
    //   receiptItem.receipt = await this.receiptRepository.findOneBy({
    //     id: re.receiptId,
    //   }); //FK
    //   receiptItem.receipt = receipt; // อ้างกลับ
    //   await this.receiptItemRepository.save(receiptItem);
    //   receipt.nameEmp = receipt.nameEmp;
    //   receipt.total = receipt.total + receiptItem.total; //รวม
    //   receipt.receive = receipt.receive; //จ่าย
    //   receipt.change = receipt.receive - receipt.total; //ถอน เงินที่จ่าย-จำนวนรวม
    // }
    // await this.receiptRepository.save(receipt);
    // return this.receiptRepository.findOne({
    //   where: { id: receipt.id },
    //   relations: ['receiptItems'],
    // });
  }

  findAll() {
    return this.receiptItemRepository.find({ relations: ['material'] });
  }

  // findOne(id: number) {
  //   return this.receiptRepository.findOne({
  //     where: { id: id },
  //     relations: ['receiptItems'],
  //   });
  // }

  async update(id: number, updateReceiptItemsDto: UpdateReceiptItemDto) {
    try {
      const updatedRecriptItem = await this.receiptItemRepository.save({
        id,
        ...updateReceiptItemsDto,
      });
      return updateReceiptItemsDto;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const receiptItem = await this.receiptItemRepository.findOneBy({ id: id });
    return this.receiptItemRepository.softRemove(receiptItem);
  }
}
