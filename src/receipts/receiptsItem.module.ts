import { Module } from '@nestjs/common';
import { ReceiptsItemsService } from './receiptsItem.service';
import { ReceiptsController } from './receipts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { ReceiptItem } from './entities/receipt-items';
import { Material } from 'src/material/entities/material.entity';
import { ReceiptsItemsController } from './receiptsItem.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Receipt, ReceiptItem, Material])],

  controllers: [ReceiptsItemsController],
  providers: [ReceiptsItemsService],
})
export class ReceiptsItemsModule {}
